import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SearchPipe } from './search';
import { StoriesComponent } from './stories/stories.component';
import { StoryService } from './story.service';
import { RoutingModule } from './routing/routing.module';
import { HttpClientModule } from '@angular/common/http';
import { StoryComponent } from './story/story.component';
import { InMemoryDataService } from './in-memory-data.service';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { AboutComponent } from './about/about.component';


@NgModule({
  declarations: [
    AppComponent,
    StoriesComponent,
    StoryComponent,
    AboutComponent,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RoutingModule,
    HttpClientModule,

    // TODO: Remove this when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, {dataEncapsulation: false, delay: 0}
    )
  ],
  providers: [StoryService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
