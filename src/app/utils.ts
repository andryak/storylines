import { isNullOrUndefined } from 'util';


export function contains<T>(collection: T[], element: T) {
  for (const e of collection) {
    if (JSON.stringify(e) === JSON.stringify(element)) {
      return true;
    }
  }
  return false;
}

export function makeChoice(text: string, goto: number, needItems?: Item[], lostItems?: Item[], gotItems?: Item[]): Choice {
  return {
    text: text,
    goto: goto,
    needItems: isNullOrUndefined(needItems) ? [] : needItems,
    lostItems: isNullOrUndefined(lostItems) ? [] : lostItems,
    gotItems: isNullOrUndefined(gotItems) ? [] : gotItems
  };
}
