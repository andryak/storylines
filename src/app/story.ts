class Story {
  id: number;
  title: string;
  author: string;
  description: string;
  pages: Page[];
}
