import { makeChoice } from '../utils';

// Items.
const plateShard: Item = {name: 'Plate shard', description: 'A large shard of a broken plate'};
const pills: Item = {name: 'Pills', description: 'A stack of white round painkillers'};
const baton: Item = {name: 'Baton', description: 'A military baton, from the corpse of the jailer'};
const gun: Item = {name: 'Gun', description: 'An heavy gun, from the corpse of the jailer'};
const uniform: Item = {name: 'Uniform', description: 'A uniform of the guards of the prison'};

// Events.
const brokenArm: Item = {name: 'Broken arm', description: 'Your arm is broken'};
const sleepingDogs: Item = {name: 'Sleeping dogs', description: 'The dogs guarding the main gate are sleeping'};

export const story3 = {
  id: 3,
  title: 'Prison Break',
  author: 'Andrea Aquino',
  description: ''
  + 'You have been trapped in the depths of an underground prison for over ten years. '
  + 'Finally, you are presented with an opportunity: the long corridor outside your jail door is cold and empty, '
  + 'is this your only chance to escape or the quickest route to death?',
  pages: [
    {
      id: 0,
      text: ''
      + 'It\'s freezing cold. You lost track of time years ago but you can tell it is late night. How couldn\'t it be '
      + 'in this buried hell? The clinging metal door of your cell opens slowly and the shivering light of a flashlight '
      + 'gets through it. A fat tired man is holding a plate of soup that looks and smell like baby puke. He is not '
      + 'your usual jailer.',
      choices: [
        makeChoice('Attack the jailer', 2),
        makeChoice('Ignore the jailer and sit in a corner', 3)
      ]
    },
    {
      id: 2,
      text: ''
      + 'You attack that saggy monster. The first punch hits his teeth. The jailer screams in pain like a pig with a knife '
      + 'planted in his throat. He reacts and pushes you towards the wall. He hits you hard. A lifetime in prison has weakened '
      + 'you body and your very soul. The plate falls to the ground and shatters spreading its content over the molded floor.',
      choices: [
        makeChoice('Keep fighting', 4),
        makeChoice('Surrender', 5, [], [], [brokenArm])
      ]
    },
    {
      id: 3,
      text: ''
      + 'You sit and the jailer leaves the plate on the ground, one step out of the door. Then, he looks at you and kicks the '
      + 'plate in your direction. The plate tumbles for a second and then shatters spreading its content on the floor. There '
      + 'are large sharp plate shards everywhere.',
      choices: [
        makeChoice('Get a shard of the plate and attack the jailer', 4),
        makeChoice('Stay silent and ignore him', 6)
      ]
    },
    {
      id: 4,
      text: ''
      + 'You attack the jailer but he is way faster than you and punches you in the stomach. You cannot breath and fall on your '
      + 'knees. The jailer kicks you in the chest and then in the knee. You fall on your back and your sight dims while he reaches '
      + 'for his gun. You hear a thundering sound, then you hear nothing at all.',
      choices: []
    },
    {
      id: 5,
      text: ''
      + 'You decide it is safer not to fight a fight you cannot win. The jailer approaches you and kicks you over and over again. '
      + 'After a few minutes you fall to the ground unconscious. You wake up a few hours later in the same place, covered in urine. '
      + 'Most likely the bastard marked his ground. The cell door is shut and it looks like there is nobody around. Your right arm '
      + 'is broken and hurts like hell.',
      choices: [
        makeChoice('Hide a shard of the plate in your pocket', 6, [], [], [plateShard]),
        makeChoice('Wait for somebody', 6)
      ]
    },
    {
      id: 6,
      text: ''
      + 'After some time, a guy in a black shirt with a long scar on his neck enters the corridor and reaches your cell. He is the '
      + 'supervisor of forced labor of this prison. He looks at you welding a pickaxe and yells "Move on, I have no time to waste".',
      choices: [
        makeChoice('Follow him', 7),
        makeChoice('Tell him about your broken arm', 8, [brokenArm])
      ]
    },
    {
      id: 7,
      text: ''
      + 'Without a single word you get on your feet and start following him. As usual he did not handcuffed you. All in all, you '
      + 'are not much of a threat. You put your hand in your pocket and exit the cell.',
      choices: [
        makeChoice('Attack the jailer with the shard', 10, [plateShard], [plateShard]),
        makeChoice('Keep following the jailer', 27)
      ]
    },
    {
      id: 8,
      text: ''
      + 'The supervisor is furious. He fills the gap between you and him and grabs your broken arm making you scream in pain. He '
      + 'looks at you and yells at the jailer: "bring this asshole to the nursery". The jailer reluctantly follows the order.',
      choices: [
        makeChoice('Go to the nursery', 9, [], [], [pills])
      ]
    },
    {
      id: 9,
      text: ''
      + 'The nursery is a pretty small room with molds on most sides and without windows. The two neon lights on the ceiling are '
      + 'the only source of light in this area. The nurse is an old lady covered in wrinkles that worked there since the beginning '
      + 'of time. After a quick check she bandages your arm, gives you a stack of painkillers and also injects you with a syringe. '
      + '"It\'s for the pain" she says. Finally, she leaves the syringe on the table and gives you her back, looking for something '
      + 'in a drawer.',
      choices: [
        makeChoice('Attack the nurse with the plate shard', 25, [plateShard], [plateShard]),
        makeChoice('Attack the nurse with the syringe', 26)
      ]
    },
    {
      id: 10,
      text: ''
      + 'You quickly get out the shard hidden in your pocket and plant it in the neck of the jailer. He tries to scream but the '
      + 'shard is way too far into his throat to make this an option. Blood rains on your face and on the surrounding walls. You '
      + 'keep pushing the shard and strangling him until he is cold.',
      choices: [
        makeChoice('Take the baton from the jailer', 12, [], [], [baton]),
        makeChoice('Take the gun from the jailer', 12, [], [], [gun]),
        makeChoice('Take the uniform of the jailer', 11),
        makeChoice('Run across the corridor', 13)
      ]
    },
    {
      id: 11,
      text: ''
      + 'You start removing the clothes of the jailer. After getting the shirt and the pants you see another jailer entering '
      + 'the corridor. He screams in terror, reaches his gun and before you can run away he hits you with a few bullets in the '
      + 'back. You fall to the ground mixing your blood with the one of your victim. You surely did not expect that death would '
      + 'be that warm and cuddling.',
      choices: []
    },
    {
      id: 12,
      text: ''
      + 'The corpse of the jailer is still on the ground. You distinctly hear somebody coming towards you from the end of the corridor.',
      choices: [
        makeChoice('Attack with the baton whoever is coming', 15, [baton]),
        makeChoice('Shoot whoever is coming', 14, [gun]),
        makeChoice('Run across the corridor', 13)
      ]
    },
    {
      id: 13,
      text: ''
      + 'You run as fast as you can across the corridor. As soon as you get to the corner you get hit in the face by a baton. You '
      + 'fall and spit blood while your assailant hits you twice again, once on your chest and once on your head. Your skull shutters '
      + 'and even your soul gets hashed by the many bone shards that once made a cage for your brain.',
      choices: []
    },
    {
      id: 14,
      text: ''
      + 'As soon as a guy with a baton enters the corridor from around the corner you start shooting and put a few bullets in his '
      + 'stomach, a few in his chest and one in an eye. His dead body crushes on the ground. Now the foot steps sounds gets stronger '
      + 'and is coming from all directions. In a few seconds you find yourself in the middle of the corridor surrounded by armed '
      + 'guards. In the blink of an eye they open fire. What remains of you is now one with the walls and the ground.',
      choices: []
    },
    {
      id: 15,
      text: ''
      + 'As soon as a guy with a baton enters the corridor from around the corner you charge with your own. You hit his head killing '
      + 'him on the spot. At the end of the corridor you can see the access to the jail yard. The light in this part of the structure '
      + 'is dim.',
      choices: [
        makeChoice('Proceed to the jail yard', 16)
      ]
    },
    {
      id: 16,
      text: ''
      + 'You reach the jail yard. The light is so dim that the many guards walking around do not notice you. On the other side of the '
      + 'yard there is the main gate of the prison, while a few meters on your right there is a small hut.',
      choices: [
        makeChoice('Hide in the hut', 18),
        makeChoice('Try to slowly reach the main gate', 17)
      ]
    },
    {
      id: 17,
      text: ''
      + 'You start moving towards the main gate hiding in shadows. You almost reach it but a guard dog notices you and starts '
      + 'barking. You panic and start running but the guards see you and free their hounds. You reach the gate but the dogs are '
      + 'too fast. They bite your legs and your neck with unmatched ferocity. You body lays dead one step away from the freedom '
      + 'you will never get.',
      choices: []
    },
    {
      id: 18,
      text: ''
      + 'You quickly get into the hut. It is very dark and full of dust and, mostly likely, spiders and mice. On the walls '
      + 'there are a few uniforms for the guards and a stock of bandages for the nursery.',
      choices: [
        makeChoice('Wear a uniform', 20, [], [], [uniform]),
        makeChoice('Look around', 19)
      ]
    },
    {
      id: 19,
      text: ''
      + 'In a corner of the hut there is a plastic pipe which distributes water to the guard dogs of the prison. The uniforms are '
      + 'pinned to the wall with rusty nails.',
      choices: [
        makeChoice('Wear a uniform', 20, [], [], [uniform]),
        makeChoice('Melt the painkillers in the water', 21, [pills], [pills], [sleepingDogs])
      ]
    },
    {
      id: 20,
      text: 'After a few minutes you have finished wearing the uniform. You look much more like a jailer than a prisoner right know.',
      choices: [
        makeChoice('Exit the hut and approach the main gain', 22)
      ]
    },
    {
      id: 21,
      text: 'You crumble your painkillers in the water pipe. You wait an hour to be sure that the dogs have drank it.',
      choices: [
        makeChoice('Wear a uniform', 20, [], [], [uniform]),
      ]
    },
    {
      id: 22,
      text: ''
      + 'You get out and hide in the shadows. Your camouflage seems to be working and you reach the main gate unnoticed. '
      + 'You feel like you are finally free but a guard with a large dog on the leash approaches you.',
      choices: [
        makeChoice('Hit the guard and run', 23, [sleepingDogs]),
        makeChoice('Ignore the guard and keep walking', 24)
      ]
    },
    {
      id: 23,
      text: ''
      + 'You quickly kick the leg of the guard hitting him right on the nerve. He starts screaming and falls to the ground '
      + 'while the dog stays put, stunned by the painkillers. You start running as fast as you can while the prison alarms '
      + 'start ringing. Tens of guards run out to catch you but you are too far away, protected by the shadows of the night '
      + 'and the trees of the forest surrounding the prison.',
      choices: []
    }
    ,
    {
      id: 24,
      text: ''
      + 'The guard looks you closely. He looks puzzled at first, then he recognises you. When he reaches for his gun you '
      + 'attack him as hard as you can but you get hit by a punch on your mouth and start bleeding. The prison alarm starts '
      + 'ringing and the other jailers point the jail lights on you. You try to run away but get hit by their automatic guns. '
      + 'Your remains will feed the worms.',
      choices: []
    },
    {
      id: 25,
      text: ''
      + 'As soon as the woman looks back you get your shard in one hand and plant it in her eye. She screams like hell and '
      + 'before you have time to react, another jailer enters the room. You can see the smoke coming out of his gun and '
      + 'then you feel a really warm river flowing on your forehead, then you feel nothing at all.',
      choices: []
    },
    {
      id: 26,
      text: ''
      + 'You grab the syringe and stab the nurse in her neck pressing as hard as you can. The nurse falls on the ground '
      + 'without making a sound. A jailer enters the room few seconds later and you also promptly stab him with the '
      + 'syringe. He goes down without even understanding what is really going on. In front of you there is an empty '
      + 'corridor that leads to the jail yard.',
      choices: [
        makeChoice('Go towards the yard', 16)
      ]
    },
    {
      id: 27,
      text: ''
      + 'Step after step you reach the inner labor camp of the prison. You get chained and forced to break rocks with an '
      + 'heavy hammer all night long, and even the day after. Day after day you will rot in that prison, until you will '
      + 'die in the solitude of your cell.',
      choices: []
    }
  ]
};
