import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoriesComponent } from '../stories/stories.component';
import { StoryComponent } from '../story/story.component';
import { AboutComponent } from '../about/about.component';


const routes: Routes = [
  {path: '', redirectTo: '/stories', pathMatch: 'full'},
  {path: 'about', component: AboutComponent},
  {path: 'stories', component: StoriesComponent},
  {path: 'story/:id', component: StoryComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule {
}
