import { InMemoryDbService } from 'angular-in-memory-web-api';
import { story1 } from './stories-db/story1';
import { story2 } from './stories-db/story2';
import { story3 } from './stories-db/story3';


export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const stories = [
      story1,
      story2,
      story3
    ];
    return {stories};
  }
}
