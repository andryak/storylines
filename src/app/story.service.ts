import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {HttpClient} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';


@Injectable()
export class StoryService {
  private storiesUrl = 'api/stories';

  constructor(private http: HttpClient) {
  }

  getStories(): Observable<Story[]> {
    return this.http.get<Story[]>(this.storiesUrl).pipe(
      tap(heroes => console.log('fetched stories')),
      catchError(this.handleError('getStories', []))
    );
  }

  getStory(id: number) {
    const url = `${this.storiesUrl}/${id}`;
    return this.http.get<Story>(url).pipe(
      tap(_ => console.log(`getStory(${id})`)),
      catchError(this.handleError<Story>(`getStory(${id})`))
    );
  }

  private handleError<T>(operation, result?: T) {
    return (error: any): Observable<T> => {
      console.error(operation, error);
      return of(result as T);
    };
  }
}
