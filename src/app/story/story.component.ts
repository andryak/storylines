import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StoryService } from '../story.service';
import { contains } from '../utils';


@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.scss']
})
export class StoryComponent implements OnInit {
  story: Story;

  // The current page.
  page: Page;

  // Items currently owned.
  items: Item[] = [];

  constructor(private route: ActivatedRoute,
              private storyService: StoryService) {
  }

  ngOnInit() {
    this.getStory();
  }

  getStory(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.storyService.getStory(id)
      .subscribe(story => {
        this.story = story;
        this.loadPage(0);
      });
  }

  choose(choice: Choice): void {
    this.updateItems(choice);
    this.loadPage(choice.goto);
  }

  reset(): void {
    // Drop all items.
    this.items.length = 0;

    // Navigate back to first page.
    this.loadPage(0);
  }

  availableChoices(): Choice[] {
    return this.page.choices.filter(choice => this.hasItems(choice.needItems));
  }

  private hasItems(items: Item[]): boolean {
    for (const item of items) {
      if (!contains(this.items, item)) {
        return false;
      }
    }
    return true;
  }

  private updateItems(choice: Choice): void {
    // Add new items without introducing duplicates.
    for (const item of choice.gotItems) {
      if (!contains(this.items, item)) {
        this.items.push(item);
      }
    }

    // Remove lost items.
    this.items = this.items.filter(item => !contains(choice.lostItems, item));
  }

  private loadPage(pageId: number): void {
    this.page = this.story.pages.find(page => page.id === pageId);
  }
}
