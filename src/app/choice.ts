class Choice {
  // The text describing this choice.
  text: string;

  // The page reached if this choice is selected.
  goto: number;

  // Items needed for this choice to be displayed.
  needItems: Item[];

  // Items lost if this choice is selected.
  lostItems: Item[];

  // Items acquired if this choice is selected.
  gotItems: Item[];
}
