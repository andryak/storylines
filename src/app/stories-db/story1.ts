import { makeChoice } from '../utils';


const emptyCup: Item = {name: 'Empty cup', description: 'A cup that once contained boiling coffee'};

export const story1 = {
  id: 1,
  title: 'Freezing',
  author: 'Andrea Aquino',
  description: 'When it gets that cold, nobody is really safe',
  pages: [
    {
      id: 0,
      text: 'You are sitting by the window. Outside the snow is still falling down. You feel very cold...',
      choices: [
        makeChoice('Make yourself a cup of coffee', 1, [], [], [emptyCup]),
        makeChoice('Go out to gather some wood', 2)
      ]
    },
    {
      id: 1,
      text: 'You accidentally spill your coffee on your face and burn to your death.',
      choices: []
    },
    {
      id: 2,
      text: 'Outside is too cold, you freeze to death.',
      choices: []
    }
  ]
};
