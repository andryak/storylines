# Storylines

Storylines is an application that allows users to read interactive books. 
An interactive book is just like a regular book, but the reader chooses how the story progresses at each page.

## Development server

Run `ng serve` to run the development server on http://localhost:4200

## Build the application

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.
